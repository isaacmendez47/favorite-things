Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?

grey

***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?

pho with some bahn mi to the side

3. Who is your favorite fictional character?

eren yeager

4. What is your favorite animal?

my cat mocha

5. What is your favorite programming language? (Hint: You can always say Python!!)

im going to go ahead and some some python
